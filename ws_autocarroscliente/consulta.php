<?php
    include('./JaxWsSoapClient.php');
    include('./Classes/Percurso.php');
    include('./Classes/Rota.php');
    include('./Classes/Rotas.php');
    
    function criarRota($strings, $rotas){
        $stringsP = explode('_', $strings);
        array_pop($stringsP);
        $rota = new Rota();
        
        foreach($stringsP as $value){
            $percurso = explode(';', $value);
            $rota->adicionarPercurso(new Percurso($percurso));
        }
        
        $rotas->adicionarRota($rota);
    }
    
    $client = new JaxWsSoapClient('http://localhost:8084/WS_autocarros/autocarros?wsdl');
    $origem = $_POST['origem'];
    $destino = $_POST['destino'];
    $horas = explode(':', $_POST['hora']);
    $hora = $horas[0];
    $minutos = $horas[1];
    
    $strings = $client->consultar(array('origem' => $origem, 'destino' => $destino, 'horas' => $hora, 'minutos' => $minutos));

    if($strings != NULL){
        $rotas = new Rotas();

        if(gettype($strings) == 'array'){
            foreach($strings as $string){
                criarRota($string, $rotas);
            }
        }
        else{
            criarRota($strings, $rotas);
        }

        $rotas->ordenarDuracao();
        $rotas->ordernarCusto();
        $rotas->imprimir();
    }
    else{
        echo '<h1>Não foram encontradas rotas que satisfizessem os requerimentos</h1>';
    }
    echo '<a href="index.php">Voltar</a>';
?>