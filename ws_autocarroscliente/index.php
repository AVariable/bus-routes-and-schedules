<?php
    include('./JaxWsSoapClient.php');
    
    function origens()
    {
        $client = new JaxWsSoapClient("http://localhost:8084/WS_autocarros/autocarros?wsdl");
        $paragens = $client->origens(array());
        
        for($i = 0; $i < sizeof($paragens); $i++)
        {
            echo '<option value="' . $paragens[$i] . '">' . $paragens[$i] . '</option>';
        }
    }
    
    function destinos()
    {
        $client = new JaxWsSoapClient("http://localhost:8084/WS_autocarros/autocarros?wsdl");
        $paragens = $client->destinos(array());
        
        for($i = 0; $i < sizeof($paragens); $i++)
        {
            echo '<option value="' . $paragens[$i] . '">' . $paragens[$i] . '</option>';
        }
    }
    
    function horas()
    {
        $hora = 8;
        $minutos = 0;

        for($i = 0; $i < 32; $i++)
        {
            if($minutos == 0)
            {
                echo '<option value="' . $hora . ':' . $minutos . '">' . $hora .':' . $minutos . '' . $minutos . '</option>';
            }
            else
            {
                echo '<option value="' . $hora . ':' . $minutos . '">' . $hora .':' . $minutos . '</option>';
            }

            if($minutos == 30)
            {
                $minutos = 0;
                $hora += 1;       
            }
            else
            {
                $minutos = 30;
            }
        }
    }
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Autocarros</title>
    </head>
    <body>
        <form action="consulta.php" method="POST">
            Origem
            <br />
            <select name="origem">
            <?php
                origens();
            ?>
            </select>
            <br />
            Destino
            <br />
            <select name="destino">
            <?php
                destinos();
            ?>
            </select>
            <br />
            Hora
            <br />
            <select name="hora">
            <?php
                horas();
            ?>
            </select>
            <br />
            <br />
            <input type="submit" value="Consultar" />
        </form>
    </body>
</html>
