<?php
    class JaxWsSoapClient extends SoapClient
    {
        public function __call($method, $arguments)
        {
            $response = parent::__call($method, $arguments);
            
            if(isset($response->return)){
                return $response->return;
            }
            return NULL;
        }
    }
?>

