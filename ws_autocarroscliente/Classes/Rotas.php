<?php
    class Rotas{
        public $rotas;
        
        public function __construct() {
            $this->rotas = array();
        }
        
        public function adicionarRota($rota){
            array_push($this->rotas, $rota);
        }
        
        public function ordenarDuracao(){
            function cmpDuracao($a, $b)
            {
                return $a->duracao > $b->duracao;
            }
            
            usort($this->rotas, "cmpDuracao");
        }
        
        public function ordernarCusto(){
            function cmpCusto($a, $b){
                return $a->custo > $b->custo;
            }
            
            usort($this->rotas, "cmpCusto");
        }
        
        public function imprimir(){
            foreach($this->rotas as $value){
                echo '<table border="1">';
                echo '<tr><td>Origem</td><td>Destino</td><td>Hora Início</td><td>Duração</td><td>Hora Chegada</td><td>Custo</td><td>Companhia</td>';
                $value->imprimir();
                echo '</table>';
                echo '<br />';
                echo 'Hora de Chegada: ' . (int)($value->horaChegada / 60) . ':' . $value->horaChegada % 60 . ' ';
                echo 'Duração Total: ' . (int)($value->duracao / 60) . ':' . $value->duracao % 60 . ' ';
                echo 'Custo Total: ' . $value->custo . '€';
                echo '<br /><br />';
            }
        }
    }
?>