<?php
    class Rota{
        public $percursos;
        public $duracao;
        public $horaChegada;
        public $custo;
        
        public function __construct(){
            $this->percursos = array();
            $this->duracao = 0;
            $this->horaChegada = 0;
            $this->custo = 0;
        }
        
        public function adicionarPercurso($percurso){
            array_push($this->percursos, $percurso);
            
            $this->custo += $percurso->custo;
            if(sizeof($this->percursos) > 1){
                $this->duracao += ($percurso->horas * 60 + $percurso->minutos) - $this->horaChegada + $percurso->duracao;
                $this->horaChegada += ($percurso->horas * 60 + $percurso->minutos) - $this->horaChegada + $percurso->duracao;
            }
            else{
                $this->horaChegada += $percurso->horasFinal * 60 + $percurso->minutosFinal;
                $this->duracao += $percurso->duracao;
            }
        }
        
        public function imprimir(){
            foreach($this->percursos as $value){
                echo '<tr>';
                $value->imprimir();
                echo '</tr>';
            }
        }
    }
?>