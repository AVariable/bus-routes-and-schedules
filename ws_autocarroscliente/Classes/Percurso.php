<?php
    class Percurso{
        public $origem;
        public $destino;
        public $horas;
        public $minutos;
        public $duracao;
        public $horasFinal;
        public $minutosFinal;
        public $custo;
        public $companhia;
        
        public function __construct($array){
            $this->origem = $array[0];
            $this->destino = $array[1];
            $this->horas = $array[2];
            $this->minutos = $array[3];
            $this->duracao = $array[4];
            $this->horasFinal = $array[5];
            $this->minutosFinal = $array[6];
            $this->custo = $array[7];
            $this->companhia = $array[8];
        }
        
        public function imprimir(){
            echo '<td>' . $this->origem . '</td>';
            echo '<td>' . $this->destino . '</td>';
            echo '<td>' . $this->horas . ':' . $this->minutos . '</td>';
            echo '<td>' . (int)($this->duracao / 60) . ':' . $this->duracao % 60 . '</td>';
            echo '<td>' . $this->horasFinal . ':' . $this->minutosFinal . '</td>';
            echo '<td>' . $this->custo . '€</td>';
            echo '<td>' . $this->companhia . '</td>';
        }
    }
?>

