/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package percursosc;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Time;
import java.util.ArrayList;
import java.util.List;
import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;

/**
 *
 * @author Antonio
 */
@WebService(serviceName = "percursosc")
public class percursosc {

    /**
     * Web service operation
     * @return Retorna uma lista de origens desta companhia
     */
    @WebMethod(operationName = "origens")
    public List origens() {
        List<String> origens = new ArrayList();
        
        DbConnect connection = new DbConnect("jdbc:derby://localhost:1527/", "dbc", "app", " ", "org.apache.derby.jdbc.ClientDriver");
        ResultSet resultSet = connection.select("SELECT DISTINCT origem FROM percurso");
        try{
            while(resultSet.next()){
                origens.add(resultSet.getString("origem"));
            }
        }
        catch(SQLException e){
            System.out.println("Error: " + e.getMessage());
        }
        
        return origens;
    }

    /**
     * Web service operation
     * @return Retorna uma lista de destinos desta companhia
     */
    @WebMethod(operationName = "destinos")
    public List destinos() {
        List<String> destinos = new ArrayList();
        
        DbConnect connection = new DbConnect("jdbc:derby://localhost:1527/", "dbc", "app", " ", "org.apache.derby.jdbc.ClientDriver");
        ResultSet resultSet = connection.select("SELECT DISTINCT destino FROM percurso");
        try{
            while(resultSet.next()){
                destinos.add(resultSet.getString("destino"));
            }
        }
        catch(SQLException e){
            System.out.println("Error: " + e.getMessage());
        }
        
        return destinos;
    }

    /**
     * Web service operation
     * @param pesquisarOrigem A origem desejada
     * @return A lista de percursos com base na origem
     */
    @WebMethod(operationName = "getOrigens")
    public List getOrigens(@WebParam(name = "pesquisarOrigem") String pesquisarOrigem) {
        List<String> percursos = new ArrayList();
        List<Integer> idRotas = new ArrayList();
        
        DbConnect connection = new DbConnect("jdbc:derby://localhost:1527/", "dbc", "app", " ", "org.apache.derby.jdbc.ClientDriver");
        ResultSet resultSet = connection.select("SELECT * FROM percurso WHERE origem=\'" + pesquisarOrigem + "\'");
        try{
            while(resultSet.next()){
                idRotas.add(resultSet.getInt("idRota"));
            }
            resultSet.close();
        }
        catch(SQLException e){
            System.out.println("Error: " + e.getMessage());
        }
        
        for(int i = 0; i < idRotas.size(); i++){
            resultSet = connection.select("SELECT * FROM percurso WHERE idRota=" + idRotas.get(i) + "");
            try{
                while(resultSet.next()){
                    String origem = resultSet.getString("origem");
                    String destino = resultSet.getString("destino");
                    Time hora = resultSet.getTime("hora");
                    Time duracao = resultSet.getTime("duracao");
                    double custo = resultSet.getDouble("custo");
                    percursosa.Percurso percurso = new percursosa.Percurso(origem, destino, hora, duracao, custo, "Varela");
                    percursos.add(percurso.deserialize());
                }
                resultSet.close();
            }
            catch(SQLException e){
                System.out.println("Error: " + e.getMessage());
            }
        }
        
        return percursos;
    }

    /**
     * Web service operation
     * @param pesquisarDestino O destino desejado
     * @return A lista de percursos com base no destino
     */
    @WebMethod(operationName = "getDestinos")
    public List getDestinos(@WebParam(name = "pesquisarDestino") String pesquisarDestino) {
        List<String> percursos = new ArrayList();
        List<Integer> idRotas = new ArrayList();
        
        DbConnect connection = new DbConnect("jdbc:derby://localhost:1527/", "dbc", "app", " ", "org.apache.derby.jdbc.ClientDriver");
        ResultSet resultSet = connection.select("SELECT * FROM percurso WHERE destino=\'" + pesquisarDestino + "\'");
        try{
            while(resultSet.next()){
                idRotas.add(resultSet.getInt("idRota"));
            }
            resultSet.close();
        }
        catch(SQLException e){
            System.out.println("Error: " + e.getMessage());
        }
        
        for(int i = 0; i < idRotas.size(); i++){
            resultSet = connection.select("SELECT * FROM percurso WHERE idRota=" + idRotas.get(i) + "");
            try{
                while(resultSet.next()){
                    String origem = resultSet.getString("origem");
                    String destino = resultSet.getString("destino");
                    Time hora = resultSet.getTime("hora");
                    Time duracao = resultSet.getTime("duracao");
                    double custo = resultSet.getDouble("custo");
                    percursosa.Percurso percurso = new percursosa.Percurso(origem, destino, hora, duracao, custo, "Varela");
                    percursos.add(percurso.deserialize());
                }
                resultSet.close();
            }
            catch(SQLException e){
                System.out.println("Error: " + e.getMessage());
            }
        }
        
        return percursos;
    }
}
