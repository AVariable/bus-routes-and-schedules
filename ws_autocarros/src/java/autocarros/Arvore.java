package autocarros;

import java.util.*;

/**
 *
 * @author Antonio
 */
public class Arvore {
    public Node cabeca;
    
    public Arvore(Node cabeca){
        this.cabeca = cabeca;
    }
    
    public void adicionarNode(Node node){
        this.cabeca.adicionarAdjacente(node);
    }
    
    public List<Node> retornarTodosNodes(){
        List<Node> listOfNodes = new ArrayList<Node>();
        adicionarTodosNodes(this.cabeca, listOfNodes);
        return listOfNodes;
    }

    private void adicionarTodosNodes(Node node, List<Node> listOfNodes) {
        if (node != null && !listOfNodes.contains(node)) {
            listOfNodes.add(node);
            List<Node> children = node.adjacentes;
            if (children != null) {
                for (Node child: children) {
                    adicionarTodosNodes(child, listOfNodes);
                }
            }
        }
    }
    
    private void retornarUnicos(Node pai, List<List<Node>> caminhos, List<Node> nodes){
        nodes.add(pai);
        if(!pai.adjacentes.isEmpty()){
            for(Node filho : pai.adjacentes){
                List<Node> aux = new ArrayList();
                aux.addAll(nodes);
                retornarUnicos(filho, caminhos, aux);
            }
        }
        
        if(!caminhos.contains(nodes)){
            caminhos.add(nodes);
        }
    }
    
    public List<List<Node>> retornarUnicos(){
        List<List<Node>> caminhos = new ArrayList();
        retornarUnicos(this.cabeca, caminhos, new ArrayList());
        return caminhos;
    }
    
    private void print(Node pai, String tab){
        for(Node node : pai.adjacentes){
            System.out.println(tab + node.cabeca.origem + " " + node.cabeca.destino);
        }
        for(Node node : pai.adjacentes){
            print(node, tab + "\t");
        }
    }
    
    public void print(){
        System.out.println(this.cabeca.cabeca.origem + " " + this.cabeca.cabeca.destino);
        print(this.cabeca, "\t");
    }
}
