/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package autocarros;

import java.util.ArrayList;
import java.util.List;
import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;

/**
 *
 * @author Antonio
 */
@WebService(serviceName = "autocarros")
public class autocarros {

    /**
     * Adiciona a uma lista de strings os objectos que são strings
     * @param strings A lista de strings
     * @param objects A lista de objectos
     */
    private void adicionarStrings(List<String> strings, List<Object> objects){
        for(int i = 0; i < objects.size(); i++){
            if(!strings.contains("" + objects.get(i)))
                strings.add("" + objects.get(i));
        }
    }
    
    /**
     * Web service operation
     * @return Retorna todas as origens de todas as companhias de autocarros
     */
    @WebMethod(operationName = "origens")
    public List origens() {
        List<String> origens = new ArrayList();
        
        // DBA
        percursosa.Percursosa_Service servicea = new percursosa.Percursosa_Service();
        percursosa.Percursosa porta = servicea.getPercursosaPort();
        adicionarStrings(origens, porta.origens());
        
        // DBB
        percursosb.Percursosb_Service serviceb = new percursosb.Percursosb_Service();
        percursosb.Percursosb portb = serviceb.getPercursosbPort();
        adicionarStrings(origens, portb.origens());
        
        // DBC
        percursosc.Percursosc_Service servicec = new percursosc.Percursosc_Service();
        percursosc.Percursosc portc = servicec.getPercursoscPort();
        adicionarStrings(origens, portc.origens());
        
        return origens;
    }

    /**
     * Web service operation
     * @return Retorna todos os destinos de todas as companhias de autocarros
     */
    @WebMethod(operationName = "destinos")
    public List destinos() {
        List<String> destinos = new ArrayList();
        
        // DBA
        percursosa.Percursosa_Service servicea = new percursosa.Percursosa_Service();
        percursosa.Percursosa porta = servicea.getPercursosaPort();
        adicionarStrings(destinos, porta.destinos());
        
        // DBB
        percursosb.Percursosb_Service serviceb = new percursosb.Percursosb_Service();
        percursosb.Percursosb portb = serviceb.getPercursosbPort();
        adicionarStrings(destinos, portb.destinos());
        
        // DBC
        percursosc.Percursosc_Service servicec = new percursosc.Percursosc_Service();
        percursosc.Percursosc portc = servicec.getPercursoscPort();
        adicionarStrings(destinos, portc.destinos());
        
        return destinos;
    }
    
    /**
     * Cria percursos apartir de uma lista de strings
     * @param strings A lista de strings
     * @return A lista de percursos
     */
    private List<percursosa.Percurso> criarPercursos(List<Object> strings){
        List<percursosa.Percurso> percursos = new ArrayList();
        
        for(int i = 0; i < strings.size(); i++){
            String string = (String)strings.get(i);
            String[] parts = string.split(";");
            percursosa.Percurso percurso = new percursosa.Percurso(parts[0], parts[1], parts[2], parts[3], parts[4], parts[5], parts[6], parts[7], parts[8]);
            percursos.add(percurso);
        }
        
        return percursos;
    }
    
    private boolean compararPercurso(percursosa.Percurso percursoA, percursosa.Percurso percursoB){
        return percursoA.origem.equals(percursoB.origem) && percursoA.destino.equals(percursoB.destino) && 
                percursoA.horas == percursoB.horas && percursoA.minutos == percursoB.minutos &&
                percursoA.custo == percursoB.custo;
    }
    
    /**
     * Web service operation
     * @param origem A origem do utente
     * @param destino O destino a que o utente deseja chegar
     * @param horas A que horas o utente estará na paragem
     * @param minutos Os minutos das horas
     * @return Retorna uma lista de rotas de acordo com o que o utente pediu
     */
    @WebMethod(operationName = "consultar")
    public List consultar(@WebParam(name = "origem") String origem, @WebParam(name = "destino") String destino, @WebParam(name = "horas") int horas, @WebParam(name = "minutos") int minutos) {     
        if(!origem.equals(destino))
        {
            // Buscar origens e destinos ao DBA
            percursosa.Percursosa_Service servicea = new percursosa.Percursosa_Service();
            percursosa.Percursosa porta = servicea.getPercursosaPort();
            List<Object> strings  = porta.getOrigens(origem);
            strings.addAll(porta.getDestinos(destino));

            // Buscar origens e destinos ao DBB
            percursosb.Percursosb_Service serviceb = new percursosb.Percursosb_Service();
            percursosb.Percursosb portb = serviceb.getPercursosbPort();
            strings.addAll(portb.getOrigens(origem));
            strings.addAll(portb.getDestinos(destino));

            // Buscar origens e destinos ao DBC
            percursosc.Percursosc_Service servicec = new percursosc.Percursosc_Service();
            percursosc.Percursosc portc = servicec.getPercursoscPort();
            strings.addAll(portc.getOrigens(origem));
            strings.addAll(portc.getDestinos(destino));
            
            // Criar lista de percursos origem e percursos em geral
            List<percursosa.Percurso> percursos = criarPercursos(strings);
            List<percursosa.Percurso> percursosOrigem = new ArrayList();
            int i;

            // Remove os percursos que são iguais
            for(i = 0; i < percursos.size(); i++){
                for(int j = i + 1; j < percursos.size(); j++){
                    if(compararPercurso(percursos.get(i), percursos.get(j))){
                        percursos.remove(j);
                    }
                }
            }

            // Separar o que são origens e remove os de origem igual ao destino introduzido pelo cliente
            i = 0;
            while(i < percursos.size()){
                if(percursos.get(i).origem.equals(origem)){
                    percursosOrigem.add(percursos.get(i));
                    percursos.remove(i);
                }
                else if (percursos.get(i).origem.equals(destino)){
                    percursos.remove(i);
                }
                else{
                    i++;
                }
            }

            // Remove as origens que são antes da hora especificada
            i = 0;
            while(i < percursosOrigem.size()){
                if(!percursosOrigem.get(i).compararTempo(horas, minutos)){
                    percursosOrigem.remove(i);
                }
                else{
                    i++;
                }
            }
            
            // Se houver percursos de origem que satisfazem os requisitos
            if(!percursosOrigem.isEmpty()){
                // Cria uma lista de árvores vazias
                List<Arvore> arvores = new ArrayList();
                
                // Cria uma árvore com um percurso de origem e cria a árvore com as suas descendências a partir da origem
                for(i = 0; i < percursosOrigem.size(); i++){
                    arvores.add(new Arvore(new Node(percursosOrigem.get(i))));
                    for(int j = 0; j < percursos.size(); j++){
                        arvores.get(i).adicionarNode(new Node(percursos.get(j)));
                    }
                }
                
                // Remove árvores com destinos errados
                i = 0;
                while(i < arvores.size()){
                    List<Node> nodes = arvores.get(i).retornarTodosNodes();
                    if(!nodes.get(nodes.size() - 1).cabeca.destino.equals(destino)){
                        arvores.remove(i);
                    }
                    else{
                        i++;
                    }
                }
                
                if(!arvores.isEmpty()){
                    List<String> caminhosS = new ArrayList();
                    
                    for(Arvore arvore : arvores){
                        List<List<Node>> nodes = arvore.retornarUnicos();

                        // Remove caminhos com destinos errados
                        i = 0;
                        while(i < nodes.size()){
                            if(!nodes.get(i).get(nodes.get(i).size() - 1).cabeca.destino.equals(destino)){
                                nodes.remove(i);
                            }
                            else{
                                i++;
                            }
                        }
                        
                        for(List<Node> caminho : nodes){
                            String temporary = "";
                            for(Node node : caminho){
                                temporary += node.cabeca.deserialize() + "_";
                            }
                            caminhosS.add(temporary);
                        }
                    }
                    
                    return caminhosS;
                }
            }
        }
        return null;
    }
}
