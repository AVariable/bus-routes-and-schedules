package autocarros;

import java.util.*;

/**
 *
 * @author Antonio
 */
public class Node {
    public percursosa.Percurso cabeca;
    public List<Node> adjacentes;
    
    public Node(percursosa.Percurso cabeca){
        this.cabeca = cabeca;
        this.adjacentes = new ArrayList();
    }
    
    public void adicionarAdjacente(Node node){
        if(this.cabeca.destino.equals(node.cabeca.origem) && this.cabeca.horasFinal <= node.cabeca.horas){
            if(this.cabeca.horasFinal == node.cabeca.horas){
                if(this.cabeca.minutosFinal <= node.cabeca.minutos){
                    this.adjacentes.add(node);
                }
            }
            else{
                this.adjacentes.add(node);
            }
            
        }
        else{
            for(Node adjacente : this.adjacentes){
                adjacente.adicionarAdjacente(node);
            }
        }
    }
}
