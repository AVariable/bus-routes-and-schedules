
package percursosb;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getDestinos complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getDestinos">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="pesquisarDestino" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getDestinos", propOrder = {
    "pesquisarDestino"
})
public class GetDestinos {

    protected String pesquisarDestino;

    /**
     * Gets the value of the pesquisarDestino property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPesquisarDestino() {
        return pesquisarDestino;
    }

    /**
     * Sets the value of the pesquisarDestino property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPesquisarDestino(String value) {
        this.pesquisarDestino = value;
    }

}
