
package percursosb;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the percursosb package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _GetDestinosResponse_QNAME = new QName("http://percursosb/", "getDestinosResponse");
    private final static QName _GetOrigensResponse_QNAME = new QName("http://percursosb/", "getOrigensResponse");
    private final static QName _OrigensResponse_QNAME = new QName("http://percursosb/", "origensResponse");
    private final static QName _DestinosResponse_QNAME = new QName("http://percursosb/", "destinosResponse");
    private final static QName _GetDestinos_QNAME = new QName("http://percursosb/", "getDestinos");
    private final static QName _Origens_QNAME = new QName("http://percursosb/", "origens");
    private final static QName _GetOrigens_QNAME = new QName("http://percursosb/", "getOrigens");
    private final static QName _Destinos_QNAME = new QName("http://percursosb/", "destinos");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: percursosb
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link Origens }
     * 
     */
    public Origens createOrigens() {
        return new Origens();
    }

    /**
     * Create an instance of {@link GetOrigens }
     * 
     */
    public GetOrigens createGetOrigens() {
        return new GetOrigens();
    }

    /**
     * Create an instance of {@link Destinos }
     * 
     */
    public Destinos createDestinos() {
        return new Destinos();
    }

    /**
     * Create an instance of {@link DestinosResponse }
     * 
     */
    public DestinosResponse createDestinosResponse() {
        return new DestinosResponse();
    }

    /**
     * Create an instance of {@link GetDestinos }
     * 
     */
    public GetDestinos createGetDestinos() {
        return new GetDestinos();
    }

    /**
     * Create an instance of {@link GetOrigensResponse }
     * 
     */
    public GetOrigensResponse createGetOrigensResponse() {
        return new GetOrigensResponse();
    }

    /**
     * Create an instance of {@link OrigensResponse }
     * 
     */
    public OrigensResponse createOrigensResponse() {
        return new OrigensResponse();
    }

    /**
     * Create an instance of {@link GetDestinosResponse }
     * 
     */
    public GetDestinosResponse createGetDestinosResponse() {
        return new GetDestinosResponse();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetDestinosResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://percursosb/", name = "getDestinosResponse")
    public JAXBElement<GetDestinosResponse> createGetDestinosResponse(GetDestinosResponse value) {
        return new JAXBElement<GetDestinosResponse>(_GetDestinosResponse_QNAME, GetDestinosResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetOrigensResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://percursosb/", name = "getOrigensResponse")
    public JAXBElement<GetOrigensResponse> createGetOrigensResponse(GetOrigensResponse value) {
        return new JAXBElement<GetOrigensResponse>(_GetOrigensResponse_QNAME, GetOrigensResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OrigensResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://percursosb/", name = "origensResponse")
    public JAXBElement<OrigensResponse> createOrigensResponse(OrigensResponse value) {
        return new JAXBElement<OrigensResponse>(_OrigensResponse_QNAME, OrigensResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DestinosResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://percursosb/", name = "destinosResponse")
    public JAXBElement<DestinosResponse> createDestinosResponse(DestinosResponse value) {
        return new JAXBElement<DestinosResponse>(_DestinosResponse_QNAME, DestinosResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetDestinos }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://percursosb/", name = "getDestinos")
    public JAXBElement<GetDestinos> createGetDestinos(GetDestinos value) {
        return new JAXBElement<GetDestinos>(_GetDestinos_QNAME, GetDestinos.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Origens }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://percursosb/", name = "origens")
    public JAXBElement<Origens> createOrigens(Origens value) {
        return new JAXBElement<Origens>(_Origens_QNAME, Origens.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetOrigens }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://percursosb/", name = "getOrigens")
    public JAXBElement<GetOrigens> createGetOrigens(GetOrigens value) {
        return new JAXBElement<GetOrigens>(_GetOrigens_QNAME, GetOrigens.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Destinos }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://percursosb/", name = "destinos")
    public JAXBElement<Destinos> createDestinos(Destinos value) {
        return new JAXBElement<Destinos>(_Destinos_QNAME, Destinos.class, null, value);
    }

}
