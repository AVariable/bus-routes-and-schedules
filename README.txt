Servidores dos itiner�rios:
	
DBA -> Apache Tomcat 7.0.41.0 -> Java DB -> Java
DBB -> GlassFish Server 4.0 -> MySQL -> Java
DBC -> GlassFish Server 4.0 -> Java DB -> Java

Servidor interm�dio:

WS_Autocarros -> Apache Tomcat 7.0.41.0 -> Java

Cliente:

WS_autocarrosCliente -> WAMP -> PHP

################################################

Cada servidor itiner�rio cont�m os seguintes servi�os:

destinos -> Retorna todos os nomes de destinos que existem na base de dados
origens -> Retorna todas os nomes de origens que existem na base de dados
getOrigens -> Retorna todos os percursos que t�m a origem igual ao que � pedido
getDestinos -> Retorna todos os percursos que t�m o destino igual ao que � pedido

O servidor interm�dio cont�m os seguintes servi�os:

origens -> Retorna todas as origens de todos os itiner�rios (retira os repetidos)
destinos -> Retorna todos os destinos de todos os itiner�rios (retira os repetidos)
consultar -> Retorna percurssos que est�o de acordo com a especifica��o do cliente

################################################

O cliente ao consumir o webservice consultar � apresentado com uma mensagem de que n�o existe nenhum percursso com base
nas especifica��es do cliente ou � apresentado com v�rias tabelas de v�rios percurssos que est�o de acordo com o que o 
cliente pediu.