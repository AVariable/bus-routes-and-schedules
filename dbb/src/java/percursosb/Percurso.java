package percursosa;

import java.io.Serializable;
import java.sql.Time;

public class Percurso implements Serializable{
    public String origem;
    public String destino;
    public int horas;
    public int minutos;
    public int duracao;
    public int horasFinal;
    public int minutosFinal;
    public double custo;
    public String companhia;

    public Percurso(String origem, String destino, Time hora, Time duracao, double custo, String companhia) {
        int minutosTotal = hora.getMinutes() + duracao.getMinutes();
                
        this.origem = origem;
        this.destino = destino;
        this.horas = hora.getHours();
        this.minutos = hora.getMinutes();
        this.duracao = duracao.getHours() * 60 + duracao.getMinutes();
        this.horasFinal = hora.getHours() + duracao.getHours();
        this.custo = custo;
        
        if(minutosTotal >= 60){
            this.horasFinal += minutosTotal / 60;
            this.minutosFinal = minutosTotal % 60;
        }
        else{
            this.minutosFinal = minutosTotal;
        }
        
        this.companhia = companhia;
    }
    
    public Percurso(String origem, String destino, String hora, String minutos, String duracao, String horasFinal, String minutosFinal, String custo, String companhia){
        this.origem = origem;
        this.destino = destino;
        this.horas = Integer.parseInt(hora);
        this.minutos = Integer.parseInt(minutos);
        this.duracao = Integer.parseInt(duracao);
        this.horasFinal = Integer.parseInt(horasFinal);
        this.minutosFinal = Integer.parseInt(minutosFinal);
        this.custo = Double.parseDouble(custo);
        this.companhia = companhia;
    }
    
    public boolean compararTempo(int horas, int minutos){
        if(horas > this.horas){
            return false;
        }
        else{
            if(horas >= this.horas){
                if(minutos > this.minutos){
                    return false;
                }
            }
        }
        return true;
    }
    
    public String deserialize(){
        String deserialized;
        
        deserialized = this.origem + ";";
        deserialized += this.destino + ";";
        deserialized += "" + this.horas + ";";
        deserialized += "" + this.minutos + ";";
        deserialized += "" + this.duracao + ";";
        deserialized += "" + this.horasFinal + ";";
        deserialized += "" + this.minutosFinal + ";";
        deserialized += "" + this.custo + ";";
        deserialized += "" + this.companhia;
        
        return deserialized;
    }
}
