package percursosb;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.ResultSet;

public class DbConnect {
    private Connection connection;
    private Statement statement;
    
    public DbConnect(String host, String dbName, String username, String password, String driver){
        try{
            try
            {
                Class.forName(driver).newInstance();
            } catch (ClassNotFoundException ex) {} 
            catch (InstantiationException ex) {} 
            catch (IllegalAccessException ex) {}
            this.connection = DriverManager.getConnection(host + dbName, username, password);
            this.statement = this.connection.createStatement();
        }
        catch(SQLException e){
            System.out.println("Error: " + e.getMessage());
        }
    }
    
    public ResultSet select(String query){
        try{
            return this.statement.executeQuery(query);
        }
        catch(SQLException e){
            System.out.println("Error: " + e.getMessage());
        }
        return null;
    }
}
